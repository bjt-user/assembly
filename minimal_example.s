# you can assemble this program with
# as hw.s -o hw.o
# and link like this
# ld hw.o -o hw.out

.section .data
    message:
        .ascii "Hello, World!\n"

.section .text
    .globl _start
_start:
    # write the message to standard output
    mov $4, %eax        # system call number for write
    mov $1, %ebx        # file descriptor for standard output
    mov $message, %ecx  # pointer to message string
    mov $14, %edx       # length of message string
    int $0x80           # invoke the system call

    # exit the program with status code 0
    mov $1, %eax        # system call number for exit
    xor %ebx, %ebx      # exit status code (0)
    int $0x80           # invoke the system call
