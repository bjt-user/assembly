xxd - make a hexdump or do the reverse.

make a hexdump from a binary:
```
xxd a.out a.xxd
```

make a hexdump from `xxd` into a binary again: (where `a.xxd` is the hexdump and `xxd.out` is the binary)
```
xxd -r a.xxd xxd.out
```

You have to make it executable though:
```
chmod +x xxd.out
```

#### autoskip

> -a | -autoskip\
Toggle autoskip: A single '*' replaces nul-lines.  Default off.

A simple hello world is 900 lines long though.

```
xxd -a a.out hexdump
```

This makes the hello world around 350 lines long.

***
